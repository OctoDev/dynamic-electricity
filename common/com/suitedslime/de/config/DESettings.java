package com.suitedslime.de.config;

import com.suitedslime.de.DynamicElectricity;
import cpw.mods.fml.common.Loader;
import net.minecraftforge.common.Configuration;

import java.io.File;

public class DESettings {

    public static final Configuration CONFIGURATION = new Configuration(new File(Loader.instance().getConfigDir(),
            DynamicElectricity.NAME + ".cfg"));

    /**
     * Auto increment config IDs
     */
    public static final int BLOCK_ID_PREFIX = 850;
    public static final int ITEM_ID_PREFIX = 11000;

    private static int NEXT_BLOCK_ID = BLOCK_ID_PREFIX;
    private static int NEXT_ITEM_ID = ITEM_ID_PREFIX;

    public static int getNextBlockID() {
        NEXT_BLOCK_ID++;
        return NEXT_BLOCK_ID;
    }

    public static int getNextItemID() {
        NEXT_ITEM_ID++;
        return NEXT_ITEM_ID;
    }

    public static void load() {
        CONFIGURATION.load();
        // Configuration settings
        CONFIGURATION.save();
    }
}
