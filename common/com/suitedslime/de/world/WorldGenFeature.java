package com.suitedslime.de.world;

import com.suitedslime.de.world.feature.FeatureBase;
import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;

import java.util.Random;

public class WorldGenFeature implements IWorldGenerator {

    public final FeatureBase featureBase;

    public WorldGenFeature(FeatureBase featureBase) {
        this.featureBase = featureBase;
    }

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunjGenerator, IChunkProvider chunkProvider) {
        featureBase.generateFeature(random, chunkX, chunkZ, world, true);
    }
}
