package com.suitedslime.de.util;

import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomItem;

public class WeightedRandomBlock extends WeightedRandomItem {

    public final int blockID;
    public final int meta;

    public WeightedRandomBlock(ItemStack itemStack) {
        super(100);
        this.blockID = itemStack.itemID;
        this.meta = itemStack.getItemDamage();
    }

    public WeightedRandomBlock(ItemStack itemStack, int weight) {
        super(weight);
        this.blockID = itemStack.itemID;
        this.meta = itemStack.getItemDamage();
    }
}
