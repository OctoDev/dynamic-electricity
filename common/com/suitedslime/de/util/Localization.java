package com.suitedslime.de.util;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class Localization {

    /**
     * Loads all the language files for a mod. Supports loading of "child" language files.
     */
    public static int loadLanguages(String languagePath, String[] languageSupported) {
        int languages = 0;

        /**
         * Load all languages
         */
        for (String language : languageSupported) {
            LanguageRegistry.instance().loadLocalization(languagePath + language + ".properties", language, false);

            if (LanguageRegistry.instance().getStringLocalization("children", language) != "") {
                try {
                    String[] children = LanguageRegistry.instance().getStringLocalization("children",
                            language).split(",");

                    for (String child : children) {
                        if (child != "" || child != null) {
                            LanguageRegistry.instance().loadLocalization(languagePath + language + ".properties",
                                    child, false);
                            languages++;
                        }
                    }
                } catch (Exception e) {
                    FMLLog.severe("Failed to load a child language file!");
                    e.printStackTrace();
                }
            }
            languages++;
        }
        return languages;
    }

    /**
     * Gets the local string of your translation based on the key given.
     */
    public static String getLocal(String key) {
        String text = LanguageRegistry.instance().getStringLocalization(key);

        if (text == null || text == "") {
            text = LanguageRegistry.instance().getStringLocalization(key, "en_US");
        }
        return text;
    }
}
