package com.suitedslime.de.util;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class DEHelper {

    public static String getLocalString(String key) {
        LanguageRegistry languageRegistry = LanguageRegistry.instance();
        String localString = languageRegistry.getStringLocalization(key);
        if (localString.equals("")) {
            localString = languageRegistry.getStringLocalization(key, "en_US");
        }
        return localString;
    }
}
