package com.suitedslime.de.helpers.tooltip;

import com.suitedslime.de.util.FormatCodes;

public class TooltipRegister {

    public final static String commentCode = FormatCodes.DarkGrey.format + FormatCodes.Italic.format;

    public static void initTooltips() {
        initBlockTooltips();
        initItemTooltips();
    }

    private static void initBlockTooltips() {

    }

    private static void initItemTooltips() {

    }
}
