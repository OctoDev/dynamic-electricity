package com.suitedslime.de;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.ModMetadata;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;

import java.util.Arrays;
import java.util.logging.Logger;

@Mod(modid = DynamicElectricity.ID, name = DynamicElectricity.NAME, version = DynamicElectricity.VERSION,
        useMetadata = true)
@NetworkMod(clientSideRequired = true, serverSideRequired = false, channels = {DynamicElectricity.CHANNEL},
        packetHandler = PacketHandler.class)
public class DynamicElectricity {

    // General variables
    public static final String CHANNEL = "DE";
    public static final String ID = "dynamicE";
    public static final String NAME = "Dynamic Electricity";
    public static final String DOMAIN = "dynamicE";
    public static final String PREFIX = DOMAIN + ":";
    public static final String MAJOR_VERSION = "@MAJOR@";
    public static final String MINOR_VERSION = "@MINOR@";
    public static final String REVISION_VERSION = "@REVIS@";
    public static final String VERSION = MAJOR_VERSION + "." + MINOR_VERSION + "." + REVISION_VERSION;

    // Directory constants
    public static final String RESOURCE_DIR = "/assets/dyamicE";
    public static final String LANGUAGE_DIR = RESOURCE_DIR + "lang/";

    public static final String TEXTURE_DIR = "textures/";
    public static final String BLOCK_DIR = TEXTURE_DIR + "blocks/";
    public static final String ITEM_DIR = TEXTURE_DIR + "item/";
    public static final String MODEL_DIR = TEXTURE_DIR + "models/";
    public static final String GUI_DIR = TEXTURE_DIR + "gui/";

    public static final Logger LOGGER = Logger.getLogger(NAME);

    @Mod.Instance(ID)
    public static DynamicElectricity instance;
    @Mod.Metadata(ID)
    public static ModMetadata metadata;
    @SidedProxy(clientSide = "com.suitedslime.de.ClientProxy", serverSide = "com.suitedslime.de.CommonProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

        // General registry
        LOGGER.setParent(FMLLog.getLogger());
        NetworkRegistry.instance().registerGuiHandler(this, proxy);
    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent event) {

        // Write metadata info
        metadata.modId = ID;
        metadata.name = NAME;
        metadata.description = "Dynamic Electricity adds a whole new energy system to Minecraft, " +
                "along with some useful tools that extends other mod's functionality.";
        metadata.url = "http://suitedslime.com";
        metadata.logoFile = "/de_logo.png";
        metadata.version = VERSION;
        metadata.authorList = Arrays.asList(new String[]{"SuitedSlime"});
        metadata.credits = "Please visit the website for further information.";
        metadata.autogenerated = false;
    }

    @Mod.EventHandler
    public void modsLoaded(FMLPostInitializationEvent event) {

    }
}
